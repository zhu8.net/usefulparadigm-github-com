---
layout: page
title: 플라타너스트리
permalink: /pltree/
group: listable
sitemap:
  priority: 0.7
  changefreq: monthly
  lastmod: 2011-09-29
---

플라타너스트리는 관심있는 책, 읽은 책, 또는 읽을 책을 온라인에서 관리하고, 좋은 책에 대한 정보를 다른 사람들과 함께 나누는 소셜 네트워킹 기반 온라인 책 관리 서비스입니다. 

[서비스 바로가기](http://pltree.com)

![플라타너스트리](/img/pages/bg_ptree_2.jpg)

**UPDATED** 이 서비스는  2011/12/31일자로 종료되었습니다.