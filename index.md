---
layout: default
title: 유스풀패러다임
tagline: 웹2.0 소프트웨어 서비스 & 솔루션
description: 웹서비스 개발/컨설팅, 페이스북앱 개발/제작, 워드프레스 테마/플러그인 제작, 오픈소스 솔루션 컨설팅/기술지원, 루비/레일스/얼랭 개발, 워드프레스 웹사이트 제작, 페이스북Facebook/트위터Twitter/소셜웹SocialWeb 개발 및 컨설팅, HTML5 모바일 웹사이트 구축
jumbotron__:
    title: Start-Up을 응원합니다!
    body: 유스풀패러다임은 새로 시작하는 비즈니스를 위한 기술지원 프로그램을 운영합니다.
services: 
  - name: webdev
    title: 웹서비스 개발/ 컨설팅
    subtitle: Web Services Dev & Consulting
    emblem: /img/emblem-rails.jpg
    description: 유스풀패러다임은 높은 생산성을 갖춘 웹 프레임워크인 <a href="http://rubyonrails.org/">루비온레일스</a>(Ruby on Rails)와 여러 오픈소스 도구를 효과적으로 활용하여 고객 니즈에 부합하는 웹서비스를 유연하고 빠르게 구현합니다.
    details: [루비/레일스 기반 웹서비스 구축, 웹서비스 기획/개발/컨설팅/프로토타이핑, 오픈API 연동/설계/구현]
  - name: wordpress
    title: 워드프레스 테마 제작
    subtitle: WordPress Theme Development
    emblem: /img/emblem-wordpress.png
    description: 유스풀패러다임은 고객 니즈에 부합하는 맞춤형 <a href="http://wordpress.org/">워드프레스</a>(WordPress) 테마를 제작하고 워드프레스 프로젝트에서 발생하는 여러 기술적 문제들을 해결해 드립니다.
    details: [맞춤형 테마(Custom Theme) 제작, API 연동 및 플러그인 제작, 워드프레스 구축 관련 컨설팅/기술지원]
    references:
    - title: Discover Hyundai
      link: http://www.discover-hyundai.com/
      thumbnail: https://usefulpa.s3.amazonaws.com/images/2014/screenshot-wp-hqtour.png
    - title: LG CES 2014
      link: http://ces2014.lgnewsroom.com/
      thumbnail: https://usefulpa.s3.amazonaws.com/images/2014/screenshot-wp-lgces2014.png
    - title: 오스터 코리아 공식 웹사이트
      link: http://www.oster.co.kr/
      thumbnail: https://usefulpa.s3.amazonaws.com/images/2014/screenshot-wp-osterkr.png
  - name: facebook
    title: 페이스북 앱 제작
    subtitle: Facebook App Development
    emblem: /img/emblem-facebook.png
    description: 유스풀패러다임은 풍부한 페이스북 플랫폼 지식과 다양한 앱 구축 경험을 토대로 고객의 마케팅 니즈에 부합하는 커스텀 앱을 신속하게 제작합니다.
    details: [100% 맞춤형 앱 설계/제작, 데스크톱/모바일 통합, 오픈그래프(Open Graph) 연동, 관리자 기능/분석도구 지원]
    references:
    - thumbnail: http://cookinfacebook.com/wp-content/uploads/2013/03/skt-mission3.jpg
      title: "SKT '좋아요' 사진 릴레이 이벤트 앱"
    - thumbnail: http://cookinfacebook.com/wp-content/uploads/2013/03/wcg-fantasy.jpg
      title: WCG 환상의 게임팀 만들기 앱
    - thumbnail: http://cookinfacebook.com/wp-content/uploads/2013/03/skt-mission1.jpg
      title: SKT 한줄 시 백일장 이벤트 앱
    - thumbnail: http://cookinfacebook.com/wp-content/uploads/2013/03/wcg-avatar.jpg
      title: WCG Avatar 만들기 앱
    - thumbnail: http://cookinfacebook.com/wp-content/uploads/2013/03/kbstar-20slist.png
      title: "KB카드 20′s List 투표 이벤트 앱"
    - thumbnail: http://cookinfacebook.com/wp-content/uploads/2013/03/niveakorea-review.jpg
      title: 니베아코리아 제품 리뷰 앱
  - name: html5mobile
    title: HTML5 모바일 웹 개발
    subtitle: HTML5 & Mobile Web Development
    emblem: /img/emblem-html5.png
    description: 유스풀패러다임은 HTML5/CSS3로 대표되는 최신 웹 기술을 기반으로 모바일 환경에 최적화된 사용성 높은 웹사이트와 웹애플리케이션을 구현합니다.
    details: [HTML5 프론트엔드 웹앱 제작, Cordova/PhoneGap 기반 하이브리드앱 제작, 반응형웹(Responsive Web) 지원, 리거시(Legacy) 웹사이트 모바일 대응]
  - name: oss
    title: 오픈소스 기술지원
    subtitle: Open Source Tech Support
    emblem: /img/emblem-oss.png
    description: 유스풀패러다임은 오랜 프로젝트 경험과 다양한 오픈소스 솔루션 지원 경험을 바탕으로 여러분 프로젝트에 가장 잘 맞는 최적의 오픈소스 솔루션을 찾아 프로젝트에 적용할 수 있도록 도와 드립니다.
    details__: [오픈소스 솔루션 소싱, 오픈소스 솔루션 도입 컨설팅, 구축/적용/기술지원, 호스팅 및 유지관리]
---

<!-- Nav tabs -->
<!--ul class="nav nav-pills nav-justifiedx">
    <li class="active"><a href="/">Home</a></li>
    <li class=""><a href="/articles.html">Articles</a></li>
</ul-->

{% include home.html %}
{% include articles.html %}
